#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2023/2/27 16:39
# @Author : sparkle_code_guy
from transformers import T5Tokenizer, T5ForConditionalGeneration

tokenizer = T5Tokenizer.from_pretrained("ClueAI/ChatYuan-large-v1")
model = T5ForConditionalGeneration.from_pretrained("ClueAI/ChatYuan-large-v1")
import torch

# 修改colab笔记本设置为gpu，推理更快
device = torch.device('cuda')
model.to(device)

def preprocess(text):
    text = text.replace("\n", "\\n").replace("\t", "\\t")
    return text

def postprocess(text):
    return text.replace("\\n", "\n").replace("\\t", "\t")

def answer(text, sample=True, top_p=1, temperature=0.7):
    '''sample：是否抽样。生成任务，可以设置为True;
    top_p：0-1之间，生成的内容越多样'''
    text = preprocess(text)
    print(len(text))
    encoding = tokenizer(text=[text], truncation=True, padding=True, max_length=768, return_tensors="pt").to(device)
    if not sample:
        out = model.generate(**encoding, return_dict_in_generate=True, output_scores=False, max_new_tokens=512,
                             num_beams=1, length_penalty=0.6)
    else:
        out = model.generate(**encoding, return_dict_in_generate=True, output_scores=False, max_new_tokens=512,
                             do_sample=True, top_p=top_p, temperature=temperature, no_repeat_ngram_size=3)
    out_text = tokenizer.batch_decode(out["sequences"], skip_special_tokens=True)
    return postprocess(out_text[0])

def rewrite_message(input):
    print("query message:",input)
    answer_message_list=[]
    for each in range(4):
        answer_message_list.append("方案{0}：".format(each) + answer_message(input) )

    return "\n\n".join(answer_message_list)

def answer_message(input):
    input_format = input.replace("\n", "。")
    input_text = "用户：" + input_format + "\n小智："
    output_text = answer(input_text)
    return f"{output_text}"

import gradio as gr

examples_list = [
# "帮我写一个请假条，我因为新冠不舒服，需要请假3天，请领导批准",
#                  "你能干什么",
#                  "写一封英文商务邮件给英国客户，表达因为物流延误，不能如期到达，我们可以赔偿贵公司所有损失",
#                  "写一个文章，题目是未来城市",
#                  "写一个诗歌，关于冬天",
#                  "从南京到上海的路线",
#                  "学前教育专业岗位实习中，在学生方面会存在问题，请提出改进措施。800字",
#                  "根据标题生成文章：标题：屈臣氏里的化妆品到底怎么样？正文：化妆品，要讲究科学运用，合理搭配。屈臣氏起码是正品连锁店。请继续后面的文字。",
#                  "帮我对比几款GPU，列出详细参数对比，并且给出最终结论",
                 " 用另外的话复述下面的文字： 在文章开始，需要纠正很多人的一个错误观念：零冷水不是为了省钱，而是为了提升生活品质。如果你是省钱最大的心态，那么接下来的内容就可以不用看了，零冷水技术对你毫无价值。"]

synthesis_interface = gr.Interface(rewrite_message,
                                   inputs=gr.components.Textbox(lines=10,interactive=True,placeholder="enter your question ..."),
                                   outputs=gr.components.Textbox(lines=10,interactive=False),
                                   cache_examples=False,
                                   title="问答",
                                   examples_per_page=5,
                                   examples=examples_list,
                                   live=False)
synthesis_interface.launch(share=False,server_name='0.0.0.0',server_port=7860)
