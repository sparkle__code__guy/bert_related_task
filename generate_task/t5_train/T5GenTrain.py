# 使用
import datetime
import json
import os

import transformers
from torch.utils.tensorboard import SummaryWriter
from transformers import T5Tokenizer, T5ForConditionalGeneration
import torch

def preprocess(text):
  text = text.replace("\n", "\\n").replace("\t", "\\t")
  return text

def postprocess(text):
  return text.replace("\\n", "\n").replace("\\t", "\t")

def train():

  lr = 1.5e-4
  num_warmup_steps = 2000
  epochs = 3
  tb_writer = SummaryWriter(log_dir="t5/summary")
  output_dir = "t5/my_model"
  batch_size = 1
  gradient_accumulation=1
  max_grad_norm = 1
  log_step = 1
  import pandas as pd
  colum_data = pd.read_csv("new_data.txt",sep='\t')
  data_json_list = json.loads(colum_data.to_json(force_ascii=False, orient="records"))

  total_steps = int(len(data_json_list) / epochs/ batch_size / gradient_accumulation)

  if not os.path.exists(output_dir):
    os.mkdir(output_dir)

  tokenizer = T5Tokenizer.from_pretrained("ClueAI/ChatYuan-large-v1")
  model = T5ForConditionalGeneration.from_pretrained("ClueAI/ChatYuan-large-v1")
  # 修改colab笔记本设置为gpu，推理更快
  model.train()
  device = torch.device('cuda')
  model.to(device)

  print('calculating total steps')

  optimizer = transformers.AdamW(model.parameters(), lr=lr, correct_bias=True)

  scheduler = transformers.get_linear_schedule_with_warmup(optimizer,
                                                           num_warmup_steps=num_warmup_steps,
                                                           num_training_steps=total_steps)
  print('starting training')
  overall_step = 0
  running_loss = 0
  for epoch in range(epochs):
    print('epoch {}'.format(epoch + 1))
    now = datetime.datetime.now()
    print('time: {}'.format(now))
    import random
    random.shuffle(data_json_list)

    for step, each in enumerate(data_json_list):
      input_ids = tokenizer(each.get("input"), return_tensors="pt").input_ids.long().to(device)
      labels = tokenizer(each.get("label"), return_tensors="pt").input_ids.long().to(device)
      outputs = model(input_ids=input_ids, labels=labels)
      loss = outputs.loss

      if gradient_accumulation > 1:
        loss = loss / gradient_accumulation

      loss.backward()
      torch.nn.utils.clip_grad_norm_(model.parameters(), max_grad_norm)

      #  optimizer step
      if (step + 1) % gradient_accumulation == 0:
        running_loss += loss.item()
        optimizer.step()
        optimizer.zero_grad()
        scheduler.step()
        overall_step += 1
        if (overall_step + 1) % log_step == 0:
          tb_writer.add_scalar('loss', loss.item(), overall_step)
      if (overall_step + 1) % log_step == 0:
        print('now time: {}:{}. Step {} of epoch {}, loss {}'.format(
          datetime.datetime.now().hour,
          datetime.datetime.now().minute,
          step + 1,
          epoch + 1,
          running_loss / log_step))
        running_loss = 0
      if step%10000==0 and step>=10000:
        if not os.path.exists(output_dir + 'model_epoch{}_step{}'.format(epoch + 1,step)):
          os.mkdir(output_dir + 'model_epoch{}_step{}'.format(epoch + 1,step))
        print('saving model for epoch {}, step {}'.format(epoch + 1,step))
        model_to_save = model.module if hasattr(model, 'module') else model
        model_to_save.save_pretrained(output_dir + 'model_epoch{}_step{}'.format(epoch + 1,step))
    print('saving model for epoch {}'.format(epoch + 1))
    if not os.path.exists(output_dir + 'model_epoch{}'.format(epoch + 1)):
      os.mkdir(output_dir + 'model_epoch{}'.format(epoch + 1))
    model_to_save = model.module if hasattr(model, 'module') else model
    model_to_save.save_pretrained(output_dir + 'model_epoch{}'.format(epoch + 1))
    print('epoch {} finished'.format(epoch + 1))

    then = datetime.datetime.now()
    print('time: {}'.format(then))
    print('time for one epoch: {}'.format(then - now))

  print('training finished')
  if not os.path.exists(output_dir + 'final_model'):
    os.mkdir(output_dir + 'final_model')
  model_to_save = model.module if hasattr(model, 'module') else model
  model_to_save.save_pretrained(output_dir + 'final_model')


print("begin train now")
train()
print("train end")
