import pandas as pd
import math
import numpy as np
import tensorflow as tf
import os
from seqeval.metrics import f1_score

from seqeval.metrics import classification_report,accuracy_score,f1_score
from tqdm import tqdm,trange
from sklearn.model_selection import train_test_split
'''
对应的依赖：
tensorflow==2.11.0
transformers==4.26.0
pandas==1.3.5
scikit-learn==1.0.2
seqeval==1.2.2
对应的中文ner数据集：链接：https://pan.baidu.com/s/1Bm_Xne3uUEYqzMV1jmVzHw?pwd=8z8j 提取码：8z8j
对应的英文ner数据集：链接：https://pan.baidu.com/s/12ZywXQ8LeDEDaul31N5s2Q?pwd=n5me 提取码：n5me
'''



df_data = pd.read_csv("data/kaggle/ner_data/ner_dataset.csv",sep=",",encoding="latin1").fillna(method='ffill')
df_data = df_data.head(1000)
tag_list=df_data.Tag.unique()

x_train,x_test=train_test_split(df_data,test_size=0.20,shuffle=False)

agg_func = lambda s: [ [w,t] for w,t in zip(s["Word"].values.tolist(),s["Tag"].values.tolist())]

x_train_grouped = x_train.groupby("Sentence #").apply(agg_func)
x_test_grouped = x_test.groupby("Sentence #").apply(agg_func)

x_train_sentences = [[s[0] for s in sent] for sent in x_train_grouped.values]
x_test_sentences = [[s[0] for s in sent] for sent in x_test_grouped.values]

x_train_tags = [[t[1] for t in tag] for tag in x_train_grouped.values]
x_test_tags = [[t[1] for t in tag] for tag in x_test_grouped.values]

MAX_LENGTH=128
BERT_MODEL="bert-base-multilingual-cased"

BATCH_SIZE=32

pad_token=0,
pad_token_segment_id=0,
sequence_a_segment_id=0,

from transformers import (
    TF2_WEIGHTS_NAME,
    BertConfig,
    BertTokenizer,
    TFBertForTokenClassification,
    create_optimizer)


label_map = {label: i for i, label in enumerate(tag_list)}
num_labels = len(tag_list) + 1
pad_token_label_id = 0

config = BertConfig.from_pretrained(BERT_MODEL,num_labels=num_labels)
tokenizer = BertTokenizer.from_pretrained(BERT_MODEL,do_lower_case=False)
#模型返回的结果shape为(batch,max_length,num_labels)
model = TFBertForTokenClassification.from_pretrained(
                BERT_MODEL,
                from_pt=bool(".bin" in BERT_MODEL),
                config=config)

#默认激活函数时tf.keras.activations.linear 也就是不做任何修改
#model.layers[-1].activation = tf.keras.activations.softmax

max_seq_length = 128


def convert_to_input(sentences, tags):
    #手动tokenize的原因时，tokenize后，生成的token长度不确定，需要手动与label对齐
    input_id_list, attention_mask_list, token_type_id_list = [], [], []
    label_id_list = []

    for x, y in tqdm(zip(sentences, tags), total=len(tags)):

        tokens = []
        label_ids = []
        #1 手动分词，并与label对齐
        for word, label in zip(x, y):
            word_tokens = tokenizer.tokenize(word)
            tokens.extend(word_tokens)
            # Use the real label id for the first token of the word, and padding ids for the remaining tokens
            label_ids.extend([label_map[label]] + [pad_token_label_id] * (len(word_tokens) - 1))
        #2 超过最大长度，手动截断
        special_tokens_count = 2
        if len(tokens) > max_seq_length - special_tokens_count:
            tokens = tokens[: (max_seq_length - special_tokens_count)]
            label_ids = label_ids[: (max_seq_length - special_tokens_count)]

        #3 tokens 添加add_special_tokens后对应的编码位置
        label_ids = [pad_token_label_id] + label_ids + [pad_token_label_id]
        inputs = tokenizer.encode_plus(tokens, add_special_tokens=True, max_length=max_seq_length)

        #4 生成对应的masks
        input_ids, token_type_ids = inputs["input_ids"], inputs["token_type_ids"]
        attention_masks = [1] * len(input_ids)

        attention_mask_list.append(attention_masks)
        input_id_list.append(input_ids)
        token_type_id_list.append(token_type_ids)

        label_id_list.append(label_ids)

    return input_id_list, token_type_id_list, attention_mask_list, label_id_list

input_ids_train,token_ids_train,attention_masks_train,label_ids_train=convert_to_input(x_train_sentences,x_train_tags)
input_ids_test,token_ids_test,attention_masks_test,label_ids_test=convert_to_input(x_test_sentences,x_test_tags)

#批量padding，由于truncating已经在convert_to_input处理过，所以此处处理无效
input_ids_train = tf.keras.utils.pad_sequences(input_ids_train,maxlen=max_seq_length,dtype="long",truncating="post",padding="post")
token_ids_train = tf.keras.utils.pad_sequences(token_ids_train,maxlen=max_seq_length,dtype="long",truncating="post",padding="post")
attention_masks_train = tf.keras.utils.pad_sequences(attention_masks_train,maxlen=max_seq_length,dtype="long",truncating="post",padding="post")
label_ids_train = tf.keras.utils.pad_sequences(label_ids_train,maxlen=max_seq_length,dtype="long",truncating="post",padding="post")


input_ids_test = tf.keras.utils.pad_sequences(input_ids_test,maxlen=max_seq_length,dtype="long",truncating="post",padding="post")
token_ids_test = tf.keras.utils.pad_sequences(token_ids_test,maxlen=max_seq_length,dtype="long",truncating="post",padding="post")
attention_masks_test = tf.keras.utils.pad_sequences(attention_masks_test,maxlen=max_seq_length,dtype="long",truncating="post",padding="post")
label_ids_test = tf.keras.utils.pad_sequences(label_ids_test,maxlen=max_seq_length,dtype="long",truncating="post",padding="post")
def example_to_features(input_ids,attention_masks,token_type_ids,y):
  return {"input_ids": input_ids,
          "attention_mask": attention_masks,
          "token_type_ids": token_type_ids},y

train_ds = tf.data.Dataset.from_tensor_slices((input_ids_train,attention_masks_train,token_ids_train,label_ids_train)).map(example_to_features).shuffle(1000).batch(32).repeat(5)


test_ds=tf.data.Dataset.from_tensor_slices((input_ids_test,attention_masks_test,token_ids_test,label_ids_test)).map(example_to_features).batch(1)

model.summary()

optimizer = tf.keras.optimizers.Adam()
loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
metric = tf.keras.metrics.SparseCategoricalAccuracy('accuracy')
model.compile(optimizer=optimizer, loss=loss, metrics=[metric])

history = model.fit(train_ds, epochs=1, validation_data=test_ds)

tf.keras.models.save_model(model,filepath="ner_model",save_format="tf")
